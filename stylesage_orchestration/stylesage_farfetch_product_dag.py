import os
import configparser
from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.bigquery_operator import BigQueryOperator
from airflow.providers.google.cloud.transfers.gcs_to_gcs import GCSToGCSOperator
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator
from airflow.providers.google.cloud.sensors.gcs import GCSObjectsWtihPrefixExistenceSensor
from functools import partial
from chb_airflow_dependencies.airflow_task_status import AirflowTaskStatus
from chb_airflow_dependencies.dag_utils import airflow_slack_alert



config_path = os.path.join(os.path.dirname(__file__), "config/config.ini")
config = configparser.ConfigParser()
config.read(config_path)

dag_config = config["dag"]
gcp_config = config["gcp"]


args = {
    'owner': dag_config["owner"],
    'depends_on_past': False,
    'start_date': days_ago(2),
    'retries': dag_config["retries"], 
    "on_failure_callback": partial(
        airflow_slack_alert,
        "slack_alert",
        "slack_airflow_comm",
        AirflowTaskStatus.failure,
    )
}

dag = DAG(dag_config["dag_id"],
          description='farfetch_data_pipeline',
          schedule_interval="0 8 * * 1",
          max_active_runs=1,
          catchup=False,
          default_args=args)

# Define dag variables
project_id = gcp_config["project_id"]
dataset = gcp_config["dataset"]
table_name = gcp_config["table_name"]
bucket = gcp_config["bucket"]

# Sensor - to check if any files have been copied to destination_bucket
gcs_file_sensor_task = GCSObjectsWtihPrefixExistenceSensor(
    task_id='gcs_file_sensor_task',
    bucket=bucket,
    prefix='farfetch/202',
    google_cloud_conn_id='google_cloud_default',
    soft_fail=True,
    timeout=10,
    dag=dag
)

# Load data from GCS to BQ
load_gcs_to_bq = GoogleCloudStorageToBigQueryOperator(
    task_id='load_gcs_to_bq',
    bucket=bucket,
    source_objects=['farfetch/*.csv'],
    destination_project_dataset_table=f'{project_id}:{dataset}.{table_name}',
    schema_object='schema/farfetch_products_schema.json',
    write_disposition='WRITE_TRUNCATE',
    source_format='csv',
    field_delimiter='|',
    skip_leading_rows=1,
    allow_quoted_newlines=True,
    ignore_unknown_values=True,
    allow_jagged_rows=True,
    dag=dag
)

copy_files_without_wildcard = GCSToGCSOperator(
    task_id="archive_csv",
    source_bucket=bucket,
    source_object="farfetch/*.csv",
    destination_bucket=bucket,
    destination_object="archive/farfetch/",
    move_object=True,
    dag=dag
)

t00 = DummyOperator(task_id='start', dag=dag)
t01 = DummyOperator(task_id='end', dag=dag)


t00 >> gcs_file_sensor_task >> load_gcs_to_bq >> copy_files_without_wildcard >> t01
